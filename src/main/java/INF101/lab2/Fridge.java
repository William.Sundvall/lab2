package INF101.lab2;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    ArrayList<FridgeItem> fridgeItems = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge(){
        return fridgeItems.size();
    }
    @Override
    public int totalSize(){
        return 20;
    }
    @Override
    public boolean placeIn(FridgeItem item){
        if (nItemsInFridge()<totalSize()){
            fridgeItems.add(item);
            return true;
        }
        else {
            return false;
        }
    }
    @Override
    public void takeOut(FridgeItem item){
        if(fridgeItems.contains(item)){
            fridgeItems.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
    }
    @Override
    public void emptyFridge(){
        fridgeItems.clear();
    }
    @Override
    public ArrayList<FridgeItem> removeExpiredFood(){
        ArrayList<FridgeItem> expired = new ArrayList<FridgeItem>();

        for (FridgeItem food : fridgeItems){
            if (food.hasExpired()){
                expired.add(food);
            }
        }
        for (FridgeItem food:expired){
            takeOut(food);
        }
    return expired;
        
    }
}
